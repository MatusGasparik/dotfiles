
" Load Plugins
source ~/.vim/plugins.vim
filetype plugin indent on

" KEY mapping
let mapleader = " "

" Escape the insert mode with:
inoremap jj <Esc>

" Add extra line with <CR> in command mode
map <CR> o<Esc>k

" Arrows are unvimlike
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" VIM-EASYMOTION
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)
" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)
" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)
" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set ttyfast

set backspace=indent,eol,start

" Tabs
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

" Show
set showcmd
set showmatch

" Search & highlight
set hlsearch
set smartcase
set ignorecase
nnoremap n nzzzv
nnoremap N Nzzzv

set clipboard=unnamedplus

" Visual settings
syntax on
set ruler
set number

" Do this otherwise no status line is shown
set laststatus=2
set noshowmode

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" Color scheme
colorscheme space-vim-dark
hi Comment ctermfg=59 ctermbg=NONE guifg=#5C6370 guibg=NONE
hi Normal     ctermbg=NONE guibg=NONE
hi LineNr     ctermbg=NONE guibg=NONE
hi SignColumn ctermbg=NONE guibg=NONE

" Leader shortcuts
nnoremap <leader>n :set nonumber!<CR>
nnoremap <leader>d :read !date<CR>
nnoremap <silent> <leader>, :nohls<CR>
nnoremap <leader>ve :edit $MYVIMRC<CR>
nnoremap <leader>rc :source $MYVIMRC<CR>
nnoremap <leader>q :bd<CR>
nnoremap <leader>Q :q<CR>
nnoremap <leader>s :set spell!<CR>
nnoremap <silent> <Leader>f :Files<CR>
nnoremap <silent> <Leader>b :Buffers<CR>
