# If not running interactively, don't do anything
[ -z "$PS1" ] && return

if [ -f $HOME/.bashrc ]; then
    source $HOME/.bashrc
fi

for DOTFILE in "$HOME"/.{function,path,env,alias}; do
    [ -f "$DOTFILE" ] && . "$DOTFILE"
done

# MacOS-specific
if [[ "$(uname -s)" == "Darwin" ]]; then
    for DOTFILE in "$HOME"/.{env,alias,function,path}.macos; do
        [ -f "$DOTFILE" ] && . "$DOTFILE"
    done
fi

unset DOTFILE

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/matus/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/matus/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/matus/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/matus/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
