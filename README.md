# My Dotfiles

## Requires

- git
- curl

## Installation

```bash
curl -Lks https://gitlab.com/MatusGasparik/dotfiles/raw/master/.bin/dotfiles-install.sh  | /bin/bash
```

## Source
This workflow is taken from Nicola Paolluci's briliant post [The best way to store your dotfiles: A bare Git repository](https://www.atlassian.com/git/tutorials/dotfiles).